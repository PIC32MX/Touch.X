/*! \file  touch_WriteData.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 5:32 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchFunctions.h"

/*! touch_WriteData - */

/*!
 *
 */
void touch_WriteData(uint8_t data) {
    uint8_t temp;
    uint8_t nop;
    uint8_t count;

    temp = data;
    clearTCK();

    for (count = 0; count < 8; count++) {
        if (temp & 0x80)
            setTDO();
        else
            clearTDO();
        setTCK();
        clearTCK();
        temp = temp << 1;
    }
}

