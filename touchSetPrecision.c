/*! \file  setPrecision.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 7:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchConstants.h"
#include "TouchAttributes.h"

/*! setPrecision - */

/*!
 *
 */
void touchSetPrecision(uint8_t precision)
{
  switch (precision)
    {
      case PREC_LOW:
        bPrecision = 1;
        break;
      case PREC_MEDIUM:
        bPrecision = 10;
        break;
      case PREC_HI:
        bPrecision = 25;
        break;
      case PREC_EXTREME:
        bPrecision = 100;
        break;
      default:
        bPrecision = 10;
        break;
    }
}
