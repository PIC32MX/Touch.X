#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Touch.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Touch.X.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=readFromS17.c touchRead.c touch_ReadData.c touch_WriteData.c touchGetX.c touchGetY.c touchInit.c touchSetPrecision.c touchDataAvailable.c touch_Bitsets.c touch_Max.c touchSetCalbrationData.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/readFromS17.o ${OBJECTDIR}/touchRead.o ${OBJECTDIR}/touch_ReadData.o ${OBJECTDIR}/touch_WriteData.o ${OBJECTDIR}/touchGetX.o ${OBJECTDIR}/touchGetY.o ${OBJECTDIR}/touchInit.o ${OBJECTDIR}/touchSetPrecision.o ${OBJECTDIR}/touchDataAvailable.o ${OBJECTDIR}/touch_Bitsets.o ${OBJECTDIR}/touch_Max.o ${OBJECTDIR}/touchSetCalbrationData.o
POSSIBLE_DEPFILES=${OBJECTDIR}/readFromS17.o.d ${OBJECTDIR}/touchRead.o.d ${OBJECTDIR}/touch_ReadData.o.d ${OBJECTDIR}/touch_WriteData.o.d ${OBJECTDIR}/touchGetX.o.d ${OBJECTDIR}/touchGetY.o.d ${OBJECTDIR}/touchInit.o.d ${OBJECTDIR}/touchSetPrecision.o.d ${OBJECTDIR}/touchDataAvailable.o.d ${OBJECTDIR}/touch_Bitsets.o.d ${OBJECTDIR}/touch_Max.o.d ${OBJECTDIR}/touchSetCalbrationData.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/readFromS17.o ${OBJECTDIR}/touchRead.o ${OBJECTDIR}/touch_ReadData.o ${OBJECTDIR}/touch_WriteData.o ${OBJECTDIR}/touchGetX.o ${OBJECTDIR}/touchGetY.o ${OBJECTDIR}/touchInit.o ${OBJECTDIR}/touchSetPrecision.o ${OBJECTDIR}/touchDataAvailable.o ${OBJECTDIR}/touch_Bitsets.o ${OBJECTDIR}/touch_Max.o ${OBJECTDIR}/touchSetCalbrationData.o

# Source Files
SOURCEFILES=readFromS17.c touchRead.c touch_ReadData.c touch_WriteData.c touchGetX.c touchGetY.c touchInit.c touchSetPrecision.c touchDataAvailable.c touch_Bitsets.c touch_Max.c touchSetCalbrationData.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/Touch.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F128B
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/readFromS17.o: readFromS17.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/readFromS17.o.d 
	@${RM} ${OBJECTDIR}/readFromS17.o 
	@${FIXDEPS} "${OBJECTDIR}/readFromS17.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/readFromS17.o.d" -o ${OBJECTDIR}/readFromS17.o readFromS17.c    --pedantic
	
${OBJECTDIR}/touchRead.o: touchRead.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchRead.o.d 
	@${RM} ${OBJECTDIR}/touchRead.o 
	@${FIXDEPS} "${OBJECTDIR}/touchRead.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchRead.o.d" -o ${OBJECTDIR}/touchRead.o touchRead.c    --pedantic
	
${OBJECTDIR}/touch_ReadData.o: touch_ReadData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_ReadData.o.d 
	@${RM} ${OBJECTDIR}/touch_ReadData.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_ReadData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_ReadData.o.d" -o ${OBJECTDIR}/touch_ReadData.o touch_ReadData.c    --pedantic
	
${OBJECTDIR}/touch_WriteData.o: touch_WriteData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_WriteData.o.d 
	@${RM} ${OBJECTDIR}/touch_WriteData.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_WriteData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_WriteData.o.d" -o ${OBJECTDIR}/touch_WriteData.o touch_WriteData.c    --pedantic
	
${OBJECTDIR}/touchGetX.o: touchGetX.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchGetX.o.d 
	@${RM} ${OBJECTDIR}/touchGetX.o 
	@${FIXDEPS} "${OBJECTDIR}/touchGetX.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchGetX.o.d" -o ${OBJECTDIR}/touchGetX.o touchGetX.c    --pedantic
	
${OBJECTDIR}/touchGetY.o: touchGetY.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchGetY.o.d 
	@${RM} ${OBJECTDIR}/touchGetY.o 
	@${FIXDEPS} "${OBJECTDIR}/touchGetY.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchGetY.o.d" -o ${OBJECTDIR}/touchGetY.o touchGetY.c    --pedantic
	
${OBJECTDIR}/touchInit.o: touchInit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchInit.o.d 
	@${RM} ${OBJECTDIR}/touchInit.o 
	@${FIXDEPS} "${OBJECTDIR}/touchInit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchInit.o.d" -o ${OBJECTDIR}/touchInit.o touchInit.c    --pedantic
	
${OBJECTDIR}/touchSetPrecision.o: touchSetPrecision.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchSetPrecision.o.d 
	@${RM} ${OBJECTDIR}/touchSetPrecision.o 
	@${FIXDEPS} "${OBJECTDIR}/touchSetPrecision.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchSetPrecision.o.d" -o ${OBJECTDIR}/touchSetPrecision.o touchSetPrecision.c    --pedantic
	
${OBJECTDIR}/touchDataAvailable.o: touchDataAvailable.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchDataAvailable.o.d 
	@${RM} ${OBJECTDIR}/touchDataAvailable.o 
	@${FIXDEPS} "${OBJECTDIR}/touchDataAvailable.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchDataAvailable.o.d" -o ${OBJECTDIR}/touchDataAvailable.o touchDataAvailable.c    --pedantic
	
${OBJECTDIR}/touch_Bitsets.o: touch_Bitsets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_Bitsets.o.d 
	@${RM} ${OBJECTDIR}/touch_Bitsets.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_Bitsets.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_Bitsets.o.d" -o ${OBJECTDIR}/touch_Bitsets.o touch_Bitsets.c    --pedantic
	
${OBJECTDIR}/touch_Max.o: touch_Max.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_Max.o.d 
	@${RM} ${OBJECTDIR}/touch_Max.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_Max.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_Max.o.d" -o ${OBJECTDIR}/touch_Max.o touch_Max.c    --pedantic
	
${OBJECTDIR}/touchSetCalbrationData.o: touchSetCalbrationData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchSetCalbrationData.o.d 
	@${RM} ${OBJECTDIR}/touchSetCalbrationData.o 
	@${FIXDEPS} "${OBJECTDIR}/touchSetCalbrationData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchSetCalbrationData.o.d" -o ${OBJECTDIR}/touchSetCalbrationData.o touchSetCalbrationData.c    --pedantic
	
else
${OBJECTDIR}/readFromS17.o: readFromS17.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/readFromS17.o.d 
	@${RM} ${OBJECTDIR}/readFromS17.o 
	@${FIXDEPS} "${OBJECTDIR}/readFromS17.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/readFromS17.o.d" -o ${OBJECTDIR}/readFromS17.o readFromS17.c    --pedantic
	
${OBJECTDIR}/touchRead.o: touchRead.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchRead.o.d 
	@${RM} ${OBJECTDIR}/touchRead.o 
	@${FIXDEPS} "${OBJECTDIR}/touchRead.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchRead.o.d" -o ${OBJECTDIR}/touchRead.o touchRead.c    --pedantic
	
${OBJECTDIR}/touch_ReadData.o: touch_ReadData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_ReadData.o.d 
	@${RM} ${OBJECTDIR}/touch_ReadData.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_ReadData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_ReadData.o.d" -o ${OBJECTDIR}/touch_ReadData.o touch_ReadData.c    --pedantic
	
${OBJECTDIR}/touch_WriteData.o: touch_WriteData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_WriteData.o.d 
	@${RM} ${OBJECTDIR}/touch_WriteData.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_WriteData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_WriteData.o.d" -o ${OBJECTDIR}/touch_WriteData.o touch_WriteData.c    --pedantic
	
${OBJECTDIR}/touchGetX.o: touchGetX.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchGetX.o.d 
	@${RM} ${OBJECTDIR}/touchGetX.o 
	@${FIXDEPS} "${OBJECTDIR}/touchGetX.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchGetX.o.d" -o ${OBJECTDIR}/touchGetX.o touchGetX.c    --pedantic
	
${OBJECTDIR}/touchGetY.o: touchGetY.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchGetY.o.d 
	@${RM} ${OBJECTDIR}/touchGetY.o 
	@${FIXDEPS} "${OBJECTDIR}/touchGetY.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchGetY.o.d" -o ${OBJECTDIR}/touchGetY.o touchGetY.c    --pedantic
	
${OBJECTDIR}/touchInit.o: touchInit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchInit.o.d 
	@${RM} ${OBJECTDIR}/touchInit.o 
	@${FIXDEPS} "${OBJECTDIR}/touchInit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchInit.o.d" -o ${OBJECTDIR}/touchInit.o touchInit.c    --pedantic
	
${OBJECTDIR}/touchSetPrecision.o: touchSetPrecision.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchSetPrecision.o.d 
	@${RM} ${OBJECTDIR}/touchSetPrecision.o 
	@${FIXDEPS} "${OBJECTDIR}/touchSetPrecision.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchSetPrecision.o.d" -o ${OBJECTDIR}/touchSetPrecision.o touchSetPrecision.c    --pedantic
	
${OBJECTDIR}/touchDataAvailable.o: touchDataAvailable.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchDataAvailable.o.d 
	@${RM} ${OBJECTDIR}/touchDataAvailable.o 
	@${FIXDEPS} "${OBJECTDIR}/touchDataAvailable.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchDataAvailable.o.d" -o ${OBJECTDIR}/touchDataAvailable.o touchDataAvailable.c    --pedantic
	
${OBJECTDIR}/touch_Bitsets.o: touch_Bitsets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_Bitsets.o.d 
	@${RM} ${OBJECTDIR}/touch_Bitsets.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_Bitsets.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_Bitsets.o.d" -o ${OBJECTDIR}/touch_Bitsets.o touch_Bitsets.c    --pedantic
	
${OBJECTDIR}/touch_Max.o: touch_Max.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touch_Max.o.d 
	@${RM} ${OBJECTDIR}/touch_Max.o 
	@${FIXDEPS} "${OBJECTDIR}/touch_Max.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touch_Max.o.d" -o ${OBJECTDIR}/touch_Max.o touch_Max.c    --pedantic
	
${OBJECTDIR}/touchSetCalbrationData.o: touchSetCalbrationData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/touchSetCalbrationData.o.d 
	@${RM} ${OBJECTDIR}/touchSetCalbrationData.o 
	@${FIXDEPS} "${OBJECTDIR}/touchSetCalbrationData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/touchSetCalbrationData.o.d" -o ${OBJECTDIR}/touchSetCalbrationData.o touchSetCalbrationData.c    --pedantic
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Touch.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_AR} $(MP_EXTRA_AR_PRE) r dist/${CND_CONF}/${IMAGE_TYPE}/Touch.X.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    
else
dist/${CND_CONF}/${IMAGE_TYPE}/Touch.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_AR} $(MP_EXTRA_AR_PRE) r dist/${CND_CONF}/${IMAGE_TYPE}/Touch.X.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
