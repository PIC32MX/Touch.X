/*! \file  touchRead.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 5:36 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchAttributes.h"
#include "TouchFunctions.h"


/*! touchRead - */

/*!
 *
 */
void touchRead()
{
    unsigned long tx, temp_x;
    unsigned long ty, temp_y;
    int datacount;
    int i;
    char szWork[16];

    tx = 0; ty = 0;
    temp_x = 0; temp_y = 0;
    datacount = 0;
    //digitalWrite(T_CS, LOW);
    clearTCS();

    for (i = 0; i < bPrecision; i++) {
        touch_WriteData(0x90);
        temp_x = touch_ReadData();

        touch_WriteData(0xD0);
        temp_y = touch_ReadData();

        if (!((temp_x > touch_Max(lXcalLeft, lXcalRight)) ||
                (temp_x == 0) ||
                (temp_y > touch_Max(lYcalTop, lYcalBottom)) ||
                (temp_y == 0))) {
            ty += temp_x;
            tx += temp_y;
            datacount++;
        }
    }

    setTCS();
    if (datacount > 0) {
        if (bOrientation == lDefaultOrientation) {
            nTouchPointX = tx / datacount;
            nTouchPointY = ty / datacount;
        } else {
            nTouchPointX = ty / datacount;
            nTouchPointY = tx / datacount;
        }
    } else {
        nTouchPointX = -1;
        nTouchPointY = -1;
    }
}
