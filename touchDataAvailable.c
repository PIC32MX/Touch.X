/*! \file  TouchDataAvailable.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 6:49 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include <stdbool.h>
#include "TouchFunctions.h"
#include "../TFT.X/MCP23S17defs.h"
#include "../TFT.X/HardwareDefs.h"


/*! TouchDataAvailable - */

/*!
 *
 */
bool touchDataAvailable( void )
{
    bool avail;
    uint16_t S17value;

    sendToS17(1, S17_IODIRA, TT_DIR | TT_IRQ_ONM);
    S17value = readFromS17(1, S17_GPIOA);
    if (S17value & TT_IRQ_ONM)
        avail = 0;
    else
        avail = 1;
    sendToS17(1, S17_IODIRA, TT_DIR & TT_IRQ_OFM);
    return avail;
}
