/*! \file  Touch.h
 *
 *  \brief Public touch definitions
 *
 *
 *  \author jjmcd
 *  \date April 13, 2014, 12:27 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TOUCH_H
#define	TOUCH_H

#include <stdint.h>
#include <stdbool.h>

#ifdef	__cplusplus
extern "C" {
#endif

void touchInit(uint8_t);
bool touchDataAvailable( void );
void touchRead() ;
int touchGetX( void );
int touchGetY( void );
void touchSetPrecision(uint8_t);
void touchSetCalbrationData(long, long, long, long );



#ifdef	__cplusplus
}
#endif

#endif	/* TOUCH_H */

