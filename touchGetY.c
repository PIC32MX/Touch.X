/*! \file  getY.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 7:12 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchConstants.h"
#include "TouchAttributes.h"


/*! getY - */

/*!
 *
 */
int touchGetY() {
    int c;

    if (bOrientation == lDefaultOrientation) {
        c = (long) ((long) (nTouchPointY - lYcalTop) * (lDispYsize)) /
                (long) (lYcalBottom - lYcalTop);
        if (c < 0)
            c = 0;
        if (c > lDispYsize)
            c = lDispYsize;
    } else {
        if (lDefaultOrientation == PORTRAIT)
            c = (long) ((long) (nTouchPointY - lXcalLeft) * (lDispXsize)) /
            (long) (lXcalRight - lXcalLeft);
        else
            c = (long) ((long) (nTouchPointY - lXcalLeft) * (-lDispXsize)) /
            (long) (lXcalRight - lXcalLeft) + (long) (lDispXsize);
        if (c < 0)
            c = 0;
        if (c > lDispXsize)
            c = lDispXsize;
    }
    return c;
}
