/*! \file  touch_ReadData.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 5:34 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchFunctions.h"
#include "../TFT.X/MCP23S17defs.h"
#include "../TFT.X/HardwareDefs.h"

/*! touch_ReadData - */

/*!
 *
 */
uint16_t touch_ReadData()
{
    uint8_t nop, count;
    uint16_t data;
    uint16_t S17value;

    data = 0;
    for (count = 0; count < 12; count++)
    {
        data <<= 1;
        setTCK();
        nop++;
        clearTCK();
        nop++;
        S17value = readFromS17(1, S17_GPIOA);
        if (S17value & TT_OUT_ONM)
            data++;
    }
    return (data);
}
