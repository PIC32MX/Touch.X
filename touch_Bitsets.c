/*! \file  bitsetsTouch.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date April 12, 2014, 8:57 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/MCP23S17defs.h"
#include "../include/HardwareDefs.h"
#define EXTERN
#include "../TFT.X/class_attributes.h"
#include "TouchConstants.h"
#include "TouchFunctions.h"

void shortDelay( int length )
{
    int i;
    for ( i=0; i<length; i++ )
        ;
}

void clearTCK( void )
{
    shortDelay(TOUCHDELAY);
    IOXnew = IOXold & TT_CLK_OFM;
    setClear( IOXnew );
    shortDelay(TOUCHDELAY);
}

void setTCK(void)
{
    shortDelay(TOUCHDELAY);
    IOXnew = IOXold | TT_CLK_ONM;
    setClear( IOXnew );
    shortDelay(TOUCHDELAY);
}

void clearTCS( void )
{
    IOXnew = IOXold & TT_CS_OFM;
    setClear( IOXnew );
}

void setTCS(void)
{
    IOXnew = IOXold | TT_CS_ONM;
    setClear( IOXnew );
}

void clearTDO( void )
{
    IOXnew = IOXold & TT_DIN_OFM;
    setClear( IOXnew );
}

void setTDO(void)
{
    IOXnew = IOXold | TT_DIN_ONM;
    setClear( IOXnew );
}


