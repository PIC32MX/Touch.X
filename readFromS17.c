/*! \file  sendToS17.c
 *
 *  \brief Write a byte to a register in a MCP23S17
 *
 *  32 bits are sent, bits 31 through 8 of the word used
 *  for the transmission.  the final  bits end up at PORT B
 *  of the MCP23S17 which is not used.
 *
 *  \author jjmcd
 *  \date December 14, 2013, 10:08 AM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include <xc.h>
#include <stdint.h>
#include "../include/HardwareDefs.h"
#include "../include/Delays.h"

#define LONGDELAY BITDELAY
#define CLOCKACTIVE 1
#define CLOCKINACTIVE 0



/*! sendToS17 - Send a word to the MCP23S17 */
/*! Send a command to the MCP23S17.  The transmission is always
 *  three bytes - first the MCP23S17 address with the read/write bit,
 *  next the register address and finally the value to write to the
 *  register.
 * \dotfile sendToS17-flow.dot "Function flow"
 *
 * \param chip - 0 for IOX 1, 1 for IOX 2
 * \param regAddr - Address of register within MCP23S17
 * \param data - Data to send to register
 * \return none
 */
uint16_t readFromS17( int chip, uint8_t regAddr )
{
    /* Using uint8_t and uint32_t because we really care about size */
    uint32_t SPIdata;
    uint8_t chipAddr;
    uint16_t data;

    if ( chip )
        chipAddr = 0x48;
    else
        chipAddr = 0x4c;

    SPIdata = ((chipAddr&0xff)<<24)|((regAddr&0xff)<<16)|((data&0xffff));
    // SPIdata is chap address with R/W bit set, and register desired
    //SPIdata = (((chipAddr&0xff)<<8))|1 | regAddr&0xff;
    // test only 0x48
    SPIdata = 0x4900 | regAddr&0xff;
    SPIdata<<=16;

#ifndef SPIPERIPH // Use SPI peripheral rather than bit-bang
    I_SCK = CLOCKINACTIVE;
    I_SO = 1;
    SPI2CONbits.ON = 0;
    I2_CS = 0;
    waitAwhile(LONGDELAY);

    int i;
    for ( i=0; i<16; i++ )
    {
        if (SPIdata & 0x80000000)
            I_SO=1;
        else
            I_SO=0;
        I_SCK = CLOCKACTIVE;
        waitAwhile(LONGDELAY);
        I_SCK = CLOCKINACTIVE;
        waitAwhile(LONGDELAY);
        SPIdata<<=1;
    }
    I_SO = 1;
    data = 0;
    for ( i=0; i<16; i++ )
    {
        data<<=1;
        I_SCK = CLOCKACTIVE;
        waitAwhile(LONGDELAY);
        if ( _RB10 )
            data++;
        I_SCK = CLOCKINACTIVE;
        waitAwhile(LONGDELAY);
    }

    I2_CS = 1;
    SPI2CONbits.ON = 1;
    return( data );
#else
    /* Set to 16 bit mode */
//    SPI2CONbits.MODE32  = 0;
//    SPI2CONbits.MODE16  = 1;
    SPI2STATbits.SPIROV = 0;

    /* Clear the transmit interrupt flag */
    IFS1bits.SPI2TXIF = 0;
    /* Wait for the transmit bufer to clear */
    while( !SPI2STATbits.SPITBE) {}
     /* Assert chip select */
    if ( chip )
        I2_CS = 0;
    else
        I1_CS = 0;
    waitAwhile(BITDELAY);
    /* Actually send the data */
    SPI2BUF = SPIdata;
    /* Wait for the transmit bufer to clear */
    while( !SPI2STATbits.SPITBE) {}
    /* Wait for TXIF */
    while ( !IFS1bits.SPI2TXIF ) {}
    /* Clear the interrupt flag */
    IFS1bits.SPI2TXIF = 0;
    /* Wait - calculated 34 but probably that is too high
     * 17 fails much (but not all) of the time, as does
     * 22.  23 works, 24 gives us a little headroom.
     * 140227 - SPI upped to 10 MHz so trying smaller delay
     * 19 seems solid. 18 doesn't work.
     *  */
//    waitAwhile(19*BITDELAY);
    while ( SPI2STATbits.SPIRBE )
        ;

    /* Now clock in 16 bits of receive data */

    data = SPI2BUF;

    /* De-assert chip select */
    if ( chip )
        I2_CS = 1;
    else
        I1_CS = 1;
    /* Return to 32 bit mode */
    SPI2CONbits.MODE32  = 1;
    SPI2CONbits.MODE16  = 1;
    return data;
#endif
}
