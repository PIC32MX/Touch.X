/*! \file  getX.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 7:11 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchConstants.h"
#include "TouchAttributes.h"

/*! getX - */

/*!
 *
 */
int touchGetX()
{
    long c;

    if (bOrientation == lDefaultOrientation) {
        c = (long) ((long) (nTouchPointX - lXcalLeft) * (lDispXsize)) /
                (long) (lXcalRight - lXcalLeft);
        if (c < 0)
            c = 0;
        if (c > lDispXsize)
            c = lDispXsize;
    } else {
        if (lDefaultOrientation == PORTRAIT)
            c = (long) ((long) (nTouchPointX - lYcalTop) * (-lDispYsize)) /
            (long) (lYcalBottom - lYcalTop) + (long) (lDispYsize);
        else
            c = (long) ((long) (nTouchPointX - lYcalTop) * (lDispYsize)) /
            (long) (lYcalBottom - lYcalTop);
        if (c < 0)
            c = 0;
        if (c > lDispYsize)
            c = lDispYsize;
    }
    return c;
}
