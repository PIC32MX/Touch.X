/*! \file  max.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 5:27 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! max - */

/*!
 *
 */
long touch_Max(long a, long b)
{
    if (b > a)
        return b;
    return a;
}
