/*! \file  TouchAttributes.h
 *
 *  \brief Private touch state
 *
 *
 *  \author jjmcd
 *  \date April 13, 2014, 12:27 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TOUCHATTRIBUTES_H
#define	TOUCHATTRIBUTES_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef EXTERN
#define EXTERN extern
#endif

// public:
EXTERN uint16_t nTouchPointX, nTouchPointY;
// private:
EXTERN long lDefaultOrientation;
EXTERN uint8_t bOrientation;
EXTERN uint8_t bPrecision;
EXTERN uint8_t bDisplayModel;
EXTERN long lDispXsize, lDispYsize, default_orientation;
EXTERN long lXcalLeft, lXcalRight, lYcalTop, lYcalBottom;

#ifdef	__cplusplus
}
#endif

#endif	/* TOUCHATTRIBUTES_H */

