/*! \file  touchSetCalbrationData.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date August 2, 2015, 2:16 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchAttributes.h"


/*! touchSetCalbrationData - */

/*!
 *
 */
void touchSetCalbrationData(long lXL, long lXR, long lYT, long lYB )
{
    lXcalLeft = lXL;
    lXcalRight = lXR;
    lYcalTop = lYT;
    lYcalBottom = lYB;

}
