/*! \file  InitTouch.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2015, 5:28 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdint.h>
#include "TouchConstants.h"
#define EXTERN
#include "TouchAttributes.h"

/*! InitTouch - */

/*!
 *
 */
void touchInit(uint8_t orientation) {

    bOrientation = orientation;
    lDefaultOrientation = CAL_S >> 31;
    lXcalLeft = CAL_XL;
    lXcalRight = CAL_XR;
    lYcalTop = CAL_YT;
    lYcalBottom = CAL_YB;
    lDispXsize = (CAL_S >> 12) & 0x0FFF;
    lDispYsize = CAL_S & 0x0FFF;
    bPrecision = 10;
    setTCS();
    setTCK();
    setTDO();
    setTCK();
}
