/*! \file  TouchConstants.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date April 12, 2014, 8:18 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TOUCHCONSTANTS_H
#define	TOUCHCONSTANTS_H

#ifdef	__cplusplus
extern "C" {
#endif


#define PORTRAIT        0
#define LANDSCAPE       1

#define PREC_LOW        1
#define PREC_MEDIUM     2
#define PREC_HI         3
#define PREC_EXTREME    4

#define CAL_XR          3900L
#define CAL_XL          300L
//#define CAL_XR          3400L
//#define CAL_XL          600L
#define CAL_YT          3875L
#define CAL_YB          365L
#define CAL_S           0x000EF13FUL

/* 1ms should be ~4189 */
#define TOUCHDELAY 1


#ifdef	__cplusplus
}
#endif

#endif	/* TOUCHCONSTANTS_H */

