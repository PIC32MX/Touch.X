/*! \file  TouchFunctions.h
 *
 *  \brief Internal touch functions
 *
 *
 *  \author jjmcd
 *  \date April 12, 2014, 8:20 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TOUCHFUNCTIONS_H
#define	TOUCHFUNCTIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

void sendToS17(int, uint8_t, uint16_t);
uint16_t readFromS17(int, uint8_t);

long touch_Max(long, long );
void touch_WriteData(uint8_t);
uint16_t touch_ReadData(void);



void setClear( uint16_t );
void shortDelay( int length );
void clearTCK( void );
void setTCK(void);
void clearTCS( void );
void setTCS(void);
void clearTDO( void );
void setTDO(void);




#ifdef	__cplusplus
}
#endif

#endif	/* TOUCHFUNCTIONS_H */

